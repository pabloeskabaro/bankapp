package com.example.vlad.bankapp.dto

import com.example.vlad.bankapp.dto.BaseCompanyData.Item28036961.Ul.Руководитель
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.json.JSONObject
import java.lang.reflect.Type

class StoryBossDeserializer : JsonDeserializer<Map<String, Руководитель>> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Map<String, Руководитель> {
        val jObject = JSONObject(json?.asJsonObject.toString())
        return toMap(jObject, Gson())
    }

    fun toMap(jObject: JSONObject, gson: Gson): Map<String, Руководитель> {
        val map = HashMap<String, Руководитель>()
        val keysItr = jObject.keys()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            val value = gson.fromJson<Руководитель>(jObject.get(key).toString(), Руководитель::class.java)
            map[key] = value
        }
        return map
    }
}
