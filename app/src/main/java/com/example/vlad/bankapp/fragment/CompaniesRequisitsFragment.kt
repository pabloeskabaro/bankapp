package com.example.vlad.bankapp.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.Api
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.TextUtils
import com.example.vlad.bankapp.dto.BaseRequisits
import kotlinx.android.synthetic.main.fragment_companies_requisites.btnSearch
import kotlinx.android.synthetic.main.fragment_companies_requisites.etINNInput
import kotlinx.android.synthetic.main.fragment_companies_requisites.tvContent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompaniesRequisitsFragment : BaseFragment() {

    private lateinit var api: Api

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_companies_requisites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        api = (activity?.application as App).api
        btnSearch.setOnClickListener {
            initData(etINNInput.text.replace("\\s".toRegex(), "").split(","))
        }
    }

    override fun refreshData() {
        initData(etINNInput.text.replace("\\s".toRegex(), "").split(","))
    }

    private fun initData(innList: List<String>) {
//        var innsString = ""
//        for (i in 0 until innList.size) {
//            innsString += if (i == 0) innList[i] else ", ${innList[i]}"
//        }
        api.getRequisites(innList).enqueue(object : Callback<BaseRequisits> {
            @SuppressLint("SetTextI18n")
            override fun onFailure(call: Call<BaseRequisits>, t: Throwable) {
                tvContent?.text = "ERROR"
            }

            override fun onResponse(call: Call<BaseRequisits>, response: Response<BaseRequisits>) {
                val items = response.body()?.items
                val mainSpannableString = SpannableStringBuilder();
                if (items != null) {
                    for (item in items) {
                        val ul = item.ul
                        if (ul != null) {
                            val mainTypeOfActivity = ul.mainTypeOfActivity
                            var mainTypeOfActivityString = ""
                            if (mainTypeOfActivity != null) {
                                mainTypeOfActivityString = mainTypeOfActivity.Код + " " + mainTypeOfActivity.Текст
                            }
                            println("TEST PRINT")
                            val text = String.format(getString(R.string.companies_requisits_main_string), ul.ИНН, ul.ОГРН, ul.КПП, ul.НаимПолнЮЛ, ul.adress?.АдресПолн ?: "",
                                    ul.capital?.СумКап ?: "", ul.Статус, ul.Статус, ul.endOfActivityDate, ul.boss?.ФИОПолн ?: "", mainTypeOfActivityString)
                            mainSpannableString.append(TextUtils.createSpannableString(text))
                            mainSpannableString.append("________________________\n\n")
                        }
                    }
                    tvContent?.text = mainSpannableString
                }
            }
        })
    }
}