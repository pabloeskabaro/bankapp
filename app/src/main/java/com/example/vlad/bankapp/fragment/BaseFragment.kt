package com.example.vlad.bankapp.fragment

import androidx.fragment.app.Fragment

abstract class BaseFragment : androidx.fragment.app.Fragment(){
    abstract fun refreshData()
}