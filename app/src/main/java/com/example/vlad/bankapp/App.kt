package com.example.vlad.bankapp

import android.app.Application
import android.bluetooth.le.AdvertisingSet
import android.provider.Settings
import com.example.vlad.bankapp.dto.ChangesDeserialize
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class App : Application() {

    lateinit var api: Api

    override fun onCreate() {
        super.onCreate()
        val client = OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(BODY))
                .build()
        val gson = GsonBuilder()
                .registerTypeAdapter(Any::class.java, ChangesDeserialize())
                .create()
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://api-fns.ru/")
                .client(client)
                .build()
        api = retrofit.create(Api::class.java)
    }
}