package com.example.vlad.bankapp.dto

import com.google.gson.annotations.SerializedName

// result generated from /json

data class BaseContractor(val items: List<Item2097710563>?) {

    data class Item2097710563(
            @SerializedName("ЮЛ")
            val ul: Ul?) {

        data class Ul(
                @SerializedName("ОГРН")
                val ogrn: String?,
                @SerializedName("ИНН")
                val inn: String?,
                @SerializedName("Позитив")
                val positive: Positive?,
                @SerializedName("Негатив")
                val negative: Negative?) {

            data class Negative(
                    @SerializedName("МассАдрес")
                    val massAdress: String?,
                    @SerializedName("Текст")
                    val text: String?)

            data class Positive(
                    @SerializedName("Лицензии")
                    val license: String?,
                    @SerializedName("Филиалы")
                    val filials: String?,
                    @SerializedName("КапБолее50тыс")
                    val capMore50k: String?,
                    @SerializedName("Текст")
                    val text: String?)
        }
    }
}
