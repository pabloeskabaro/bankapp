package com.example.vlad.bankapp.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.Api
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.activity.MainActivity
import com.example.vlad.bankapp.dto.BaseCompanyData
import com.example.vlad.bankapp.dto.BaseCompanyData.Item28036961.Ul.License
import com.example.vlad.bankapp.dto.BaseCompanyData.Item28036961.Ul.История
import com.example.vlad.bankapp.dto.BaseCompanyData.Item28036961.Ul.ОткрСведения
import com.example.vlad.bankapp.dto.BaseCompanyData.Item28036961.Ul.Участия2080825467
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.fragment_check_contragent.etINNInput
import kotlinx.android.synthetic.main.fragment_company_data.btnSearch
import kotlinx.android.synthetic.main.fragment_company_data.tvContent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompanyDataFragment : BaseFragment() {

    private var inn: String = ""

    lateinit var api: Api

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_company_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setToolbarTitle("Данные о компании")
        api = (activity?.application as App).api
        btnSearch.setOnClickListener {
            inn = etINNInput.text.toString()
            initData(inn)
        }
    }

    override fun refreshData() {
        if (!TextUtils.isEmpty(inn)) initData(inn)
    }

    private fun initData(inn : String) {
        api.getCopanyData(inn, (activity as MainActivity).getKey()).enqueue(object : Callback<BaseCompanyData> {
            @SuppressLint("SetTextI18n")
            override fun onFailure(call: Call<BaseCompanyData>, t: Throwable) {
                tvContent?.text = "ERROR"
            }

            override fun onResponse(call: Call<BaseCompanyData>, response: Response<BaseCompanyData>) {
                val ul = response.body()?.items?.get(0)?.ul
                if (ul != null) {
                    val no = ul.no
                    val boss = ul.boss
                    val members = ul.Учредители
                    val mainTypeOfActivity = ul.doingType
                    val secondaryTypeOfActivity = ul.ДопВидДеят
                    val writeToEgrulReasons = ul.СПВЗ
                    val filialsData = ul.filials
                    val openInfo = ul.openInfo
                    var noString = ""
                    var bossString = ""
                    var membersInfoString = ""
                    var mainTypeOfActivityString = ""
                    var secondaryTypeOfActivityString = ""
                    var writeToEgrulReasonsString = ""
                    var filialsString = ""
                    var openInfoString = ""
                    val licenseString = createLicensesString(licenses = ul.license)
                    val participantsString = createParticipants(participants = ul.Участия)
                    val historyString = createHistoryString(historyInfo = ul.story)
                    if (openInfo != null) {
                        openInfoString = createOpenInfoString(openInfo)
                    }
                    if (filialsData != null) {
                        for (filial in filialsData) {
                            val currentFilial = "${filial.type} ${filial.adress}"
                            filialsString += if (!TextUtils.isEmpty(filialsString)) ";\n$currentFilial" else currentFilial
                        }
                    }
                    if (writeToEgrulReasons != null) {
                        for (reason in writeToEgrulReasons) {
                            val currentReasonString = "${reason.Дата} ${reason.Текст}"
                            writeToEgrulReasonsString += if (!TextUtils.isEmpty(writeToEgrulReasonsString)) ";\n$currentReasonString" else currentReasonString
                        }
                    }
                    if (secondaryTypeOfActivity != null) {
                        for (typeOfActivity in secondaryTypeOfActivity) {
                            val currentTypeOfActivityString = "${typeOfActivity.Код} ${typeOfActivity.Текст}"
                            secondaryTypeOfActivityString += if (!TextUtils.isEmpty(
                                            secondaryTypeOfActivityString)) ";\n$currentTypeOfActivityString" else currentTypeOfActivityString
                        }
                    }
                    if (boss != null) {
                        bossString = "Должность: " + boss.Должн + "\nДата внесения в ЕГРЮЛ сведений о руководителе: " + boss.Дата + "\nИНН должностного лица: " + boss.ИННФЛ + "\nФИО: " + boss.ФИОПолн
                    }
                    if (no != null) {
                        noString = "${no.Рег} ${no.РегДата} ${no.Учет} ${no.УчетДата}"
                    }
                    if (mainTypeOfActivity != null) {
                        mainTypeOfActivityString = mainTypeOfActivity.Код + " " + mainTypeOfActivity.Текст
                    }
                    println("TEST PRINT")
                    if (members != null) {
                        for (m in members) {
                            val memberFl = m.uchrFl;
                            var memberFlString = ""
                            if (memberFl != null) {
                                memberFlString = "\nФИО: " + memberFl.ФИОПолн + "\nИНН: " + memberFl.ИННФЛ
                            }
                            val currentMemberString = "\n$memberFlString\nДата внесения в ЕГРЮЛ сведений об учредителе (участнике):  ${m.Дата}\nРазмер доли (в процентах): ${m.Процент}%\nНоминальная стоимость доли в рублях: ${m.СуммаУК}"
                            membersInfoString += if (!TextUtils.isEmpty(membersInfoString)) ";\n$currentMemberString" else currentMemberString
                        }
                    }
                    val text = String.format(getString(R.string.company_data_main_string), ul.inn, ul.НаимПолнЮЛ, ul.Адрес?.fullName ?: "", ul.Статус, "", noString, bossString,
                            membersInfoString, ul.phoneNumber?: "", ul.email?: "", mainTypeOfActivityString, secondaryTypeOfActivityString,
                            writeToEgrulReasonsString, filialsString, openInfoString, licenseString, participantsString, historyString)
                    val spannableString = com.example.vlad.bankapp.TextUtils.createSpannableString(text)
                    tvContent?.text = spannableString
                }
            }
        })
    }

    private fun createHistoryString(historyInfo: История?): String {
        if (historyInfo != null) {
            val historyPhones = historyInfo.phoneNumber
            val historyEmails = historyInfo.email
            val historyAdresses = historyInfo.Адрес
            val historyBosses = historyInfo.boss
            var bossesString = "Руководители: "
            var emailsString = "Имейлы: "
            var phonesString = "Телефоны: "
            var adressesString = "Адреса: "
            historyBosses?.forEach { (key, value) ->
                bossesString += "\n$key : ${value.ФИОПолн}, ${value.ИННФЛ}, ${value.Должн}, ${value.Дата}"
            }
            for (key in historyAdresses?.keySet() ?: JsonObject().keySet()) adressesString += "\n$key : ${historyAdresses!![key]}"
            for (key in historyPhones?.keySet() ?: JsonObject().keySet()) phonesString += "\n$key : ${historyPhones!![key]}"
            for (key in historyEmails?.keySet() ?: JsonObject().keySet()) emailsString += "\n$key : ${historyEmails!![key]}"
            return "$bossesString\n$emailsString\n$phonesString\n$adressesString"
        } else return ""
    }

    private fun createParticipants(participants: List<Участия2080825467>?): String {
        var participantsString = ""
        val size = participants?.size ?: 0
        for (i in 1 until size) {
            val participant = participants!![i - 1]
            participantsString += "\n1. ОГРН: ${participant.ОГРН}" +
                    "\nИНН: ${participant.ИНН}" +
                    "\nНаименование ЮР лица: ${participant.НаимПолнЮЛ}" +
                    "\nCтатус: ${participant.Статус}" +
                    "\nПроцент: ${participant.Процент}" +
                    "\nСумма УК: ${participant.СуммаУК}"
        }
        return participantsString
    }

    private fun createLicensesString(licenses: List<License>?): String {
        var licenseString = ""
        val size = (licenses?.size) ?: 0
        for (i in 1 until size) {
            val license = licenses!![i - 1]
            licenseString += "\n${i + 1}. Номер лицензии: ${license.НомерЛиц ?: ""}" +
                    "\nВид деятельности: ${license.ВидДеятельности ?: ""}" +
                    "\nДата начала: ${license.ДатаНачала ?: ""}" +
                    "\nДата окончания: ${license.ДатаОконч ?: ""}" +
                    "\nМесто действия: ${license.МестоДейств ?: ""}"
        }
        return licenseString
    }

    private fun createOpenInfoString(openInfo: ОткрСведения): String {
        var openInfoString = ""
        val industryIndications = openInfo.industryIndications
        val taxs = openInfo.taxs
        var taxsString = ""
        if (taxs != null) {
            for (tax in taxs) {
                val currentTaxString = "Имя налога : ${tax.taxName ?: ""}, " +
                        "\nПени: ${tax.fine ?: ""}," +
                        "\nСумма уплаченного налога: ${tax.paidSum ?: ""}," +
                        "\nСумма недоимки по налогу: ${tax.nonPaidSum ?: ""}, " +
                        "\nСумма задолженности по пеням: ${tax.peniSum ?: ""}," +
                        "\nСумма штрафа из-за недоимки по налогу: ${tax.fine ?: ""}, " +
                        "\nОбщая сумма недоимки по налогу, пени и штрафу: ${tax.nonPaidFineSum ?: ""}"
                taxsString += if (!TextUtils.isEmpty(taxsString)) ";\n$currentTaxString" else currentTaxString
            }
        }
        if (industryIndications != null) {
            val industryIndicationsString = "налоговая нагрузка - ${industryIndications.НалогНагрузка ?: ""}; рентабельность - ${industryIndications.Рентабельность ?: ""}"
            openInfoString += "Количество работников: ${openInfo.КолРаб ?: ""}" +
                    "\nСведения СНР: ${openInfo.СведСНР ?: ""}" +
                    "\nПризнаки участия КГН: ${openInfo.ПризнУчКГН ?: ""}" +
                    "\nНалоги: $taxsString" +
                    "\nСуммарный доход: ${openInfo.СумДоход ?: ""}" +
                    "\nСуммарный расход: ${openInfo.СумРасход ?: ""}" +
                    "\nОтраслевые показатели: $industryIndicationsString" +
                    "\nДата: ${openInfo.Дата ?: ""}"
        }
        return openInfoString
    }
}
