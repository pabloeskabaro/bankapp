package com.example.vlad.bankapp

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan

class TextUtils private constructor(){

    companion object {
        fun createSpannableString(string: String): Spannable {
            val arr = string.split("\n\n")
            val resultSpannable = SpannableStringBuilder("")
            for (arrayString in arr) {
                val localSpannable = SpannableString(arrayString + "\n\n")
                localSpannable.setSpan(ForegroundColorSpan(Color.BLACK), 0, localSpannable.indexOf(':'), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                resultSpannable.append(localSpannable)
            }
            return resultSpannable
        }

        val KEY = "KEY"

        val DEFAULT_KEY = "увавып124214"
    }
}