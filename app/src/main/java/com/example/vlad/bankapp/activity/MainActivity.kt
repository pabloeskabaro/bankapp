package com.example.vlad.bankapp.activity

import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout.DrawerListener
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.R.string
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.toolbar
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.view.Menu
import android.view.inputmethod.InputMethodManager
import com.example.vlad.bankapp.TextUtils
import android.widget.EditText
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.fragment.BaseFragment

class MainActivity : AppCompatActivity(), DrawerListener {

    private var checkedId = 0

    private val TAG = "MainActivity"

    private lateinit var navController: NavController

    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navController = Navigation.findNavController(this, R.id.fragment_container)
        setSupportActionBar(toolbar)
        initDrawerLayout()
        initPreferences()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.changeKey -> {
                val dialogView = layoutInflater.inflate(R.layout.alert_dialog_layout, null)
                val builder = AlertDialog.Builder(this)
                        .setTitle("Введите новый ключ доступа")
                        .setView(dialogView)
                val etDialog = dialogView.findViewById<EditText>(R.id.etKey)
                builder.setPositiveButton("Выбрать") { _, _ ->
                    val text = etDialog.text
                    if (!android.text.TextUtils.isEmpty(text)) {
                        preferences.edit().putString(TextUtils.KEY, text.toString()).apply()
                        updateCurrentFragment()
                    }
                }.create().show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDrawerStateChanged(p0: Int) {
    }

    override fun onDrawerSlide(p0: View, p1: Float) {
        val focusedView = currentFocus
        if (focusedView != null) {
            hideKeyboardFrom(focusedView)
            focusedView.clearFocus()
        }
    }

    override fun onDrawerClosed(p0: View) {
    }

    override fun onDrawerOpened(p0: View) {
    }

    fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    fun getKey(): String = preferences.getString(TextUtils.KEY, TextUtils.DEFAULT_KEY)

    private fun updateCurrentFragment() {
        (supportFragmentManager.primaryNavigationFragment?.childFragmentManager?.primaryNavigationFragment as BaseFragment).refreshData()
    }

    private fun initPreferences() {
        preferences = getSharedPreferences(getString(R.string.key_preferences), Context.MODE_PRIVATE)
        if (!preferences.contains(TextUtils.KEY)) {
            preferences.edit().putString(TextUtils.KEY, TextUtils.DEFAULT_KEY).apply()
        }
    }

    private fun hideKeyboardFrom(view: View) {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun initDrawerLayout() {
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar,
                string.navigation_drawer_open,
                string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        drawerLayout.addDrawerListener(this)
        toggle.syncState()
        NavigationUI.setupWithNavController(navView, navController)
    }
}