package com.example.vlad.bankapp.dto

import com.example.vlad.bankapp.dto.BaseTrackingChanges.Items1547949463.Ul.Change
import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.json.JSONObject
import java.lang.reflect.Type

class ChangesDeserialize : JsonDeserializer<Map<String, Change>> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Map<String, Change>? {
        val jObject = JSONObject(json?.asJsonObject.toString())
        return toMap(jObject, Gson())
    }

    fun toMap(jObject: JSONObject, gson: Gson): Map<String, Change> {
        val map = HashMap<String, Change>()
        val keysItr = jObject.keys()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            val value = gson.fromJson<Change>(jObject.get(key).toString(), Change::class.java)
            map[key] = value
        }
        return map
    }
}
