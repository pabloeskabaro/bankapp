package com.example.vlad.bankapp.dto

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import org.json.JSONObject
import java.lang.reflect.Type

class StoryStringDeserializer : JsonDeserializer<JSONObject> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): JSONObject {
        return JSONObject(json?.asJsonObject.toString())
    }
}