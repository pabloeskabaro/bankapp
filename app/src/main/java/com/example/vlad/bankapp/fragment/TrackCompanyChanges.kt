package com.example.vlad.bankapp.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.Api
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.TextUtils
import com.example.vlad.bankapp.activity.MainActivity
import com.example.vlad.bankapp.dto.BaseTrackingChanges
import kotlinx.android.synthetic.main.fragment_check_contragent.etINNInput
import kotlinx.android.synthetic.main.fragment_company_data.btnSearch
import kotlinx.android.synthetic.main.fragment_company_data.tvContent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrackCompanyChanges : BaseFragment() {

    private var inn : String = ""

    private val TAG: String = javaClass.simpleName

    private lateinit var api: Api

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trackingchanges, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setToolbarTitle("Отслеживание изменений")
        api = (activity?.application as App).api
        btnSearch.setOnClickListener {
            initData(etINNInput.text.toString())
        }
    }

    override fun refreshData() {
        if (!android.text.TextUtils.isEmpty(inn)) initData(inn)
    }

    private fun initData(inn : String) {
        api.getChanges(inn, (activity as MainActivity).getKey()).enqueue(object : Callback<BaseTrackingChanges> {
            override fun onFailure(call: Call<BaseTrackingChanges>, t: Throwable) {
                Log.d(TAG, t.message)
                tvContent?.text = "ERROR"
            }

            override fun onResponse(call: Call<BaseTrackingChanges>, response: Response<BaseTrackingChanges>) {
                val ul = response.body()?.items?.get(0)?.ul ?: return
                val innString = ul.inn
                val ogrn = ul.ogrn
                val changes = ul.changes
                var changesString = ""
                changes?.forEach { (key, value) ->
                    changesString += "\n$key :"
                    val status = value.status
                    if (status != null) changesString += "\n$status"
                    val boss = value.boss
                    if (boss != null) changesString += "\nРуководитель - ${boss.innfl} ${boss.fullName}"
                    val adress = value.adress
                    if (adress != null) changesString += "\nАдрес - ${adress.fullAdress}"
                    val fssInfo = value.fssInfo
                    if (fssInfo != null) changesString += "\nФСС - ${fssInfo.fssRegNumber}"
                    val spvz = value.spvz
                    if (spvz != null) changesString += "Сведения о причинах внесения записей в реестр - \n$spvz"
                    val taxInfo = value.taxInfo
                    if (taxInfo != null) changesString += "Информация о налоговых органах - " +
                            "Код и наименование налогового органа, куда ЮЛ встало на учет: ${taxInfo.accounting} " +
                            "Код и наименование регистрирующего (налогового) органа: ${taxInfo.reg}"
                }
                val text = TextUtils.createSpannableString(String.format(getString(R.string.track_changes_main_string),
                        innString, ogrn, changesString))
                tvContent?.text = text
            }
        })
    }
}