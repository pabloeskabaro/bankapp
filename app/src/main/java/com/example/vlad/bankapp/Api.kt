package com.example.vlad.bankapp

import com.example.vlad.bankapp.dto.BaseCompanyData
import com.example.vlad.bankapp.dto.BaseContractor
import com.example.vlad.bankapp.dto.BaseRequestStat
import com.example.vlad.bankapp.dto.BaseRequisits
import com.example.vlad.bankapp.dto.BaseTrackingChanges
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("https://api-fns.ru/api/egr")
    fun getCopanyData(@Query("req") inn : String, @Query ("key") key : String): Call<BaseCompanyData>

    @GET("https://api-fns.ru/api/check")
    fun checkContractor(@Query("req") inn : String, @Query ("key") key : String): Call<BaseContractor>

    @GET("https://api-fns.ru/api/changes")
    fun getChanges(@Query("req") inn : String, @Query ("key") key : String): Call<BaseTrackingChanges>

    @GET("https://damia.ru/api-scoring/score?inn=6663003127&model=myModel1&key=ba450bd806b09e1700cb52d59591c12c52f4ab61")
    fun getScoring(): Call<Any>

    @GET("https://api-fns.ru/api/innfl?key=ba450bd806b09e1700cb52d59591c12c52f4ab61")
    fun getInnTopManager(@Query("fam") surname: String, @Query("nam") name: String, @Query("otch") patronymic: String,
            @Query("bdate") birthDate: String, @Query("doctype") docType: String?, @Query("docno") docNumber: String): Call<Any>

    @GET("https://api-fns.ru/api/stat")
    fun getRequestStat(@Query ("key") key : String): Call<BaseRequestStat>

    @GET("https://api-fns.ru/api/multinfo?key=ba450bd806b09e1700cb52d59591c12c52f4ab61")
    fun getRequisites(@Query("req[]") inns: List<String>) : Call<BaseRequisits>
}