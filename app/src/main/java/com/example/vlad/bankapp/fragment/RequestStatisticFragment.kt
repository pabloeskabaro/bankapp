package com.example.vlad.bankapp.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.Api
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.TextUtils
import com.example.vlad.bankapp.activity.MainActivity
import com.example.vlad.bankapp.dto.BaseRequestStat
import kotlinx.android.synthetic.main.fragment_company_data.tvContent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestStatisticFragment : BaseFragment() {

    private val TAG: String = javaClass.simpleName

    private lateinit var api: Api

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_request_statistic, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setToolbarTitle("Реквизиты группы компании")
        api = (activity?.application as App).api
        initData()
    }

    override fun refreshData() {
        initData()
    }

    private fun initData() {
        api.getRequestStat((activity as MainActivity).getKey()).enqueue(object : Callback<BaseRequestStat> {
            override fun onFailure(call: Call<BaseRequestStat>, t: Throwable) {
                Log.d(TAG, t.message)
                tvContent?.text = "ERROR"
            }

            override fun onResponse(call: Call<BaseRequestStat>, response: Response<BaseRequestStat>) {
                val methods = response.body()?.methods
                if (methods != null) {
                    val changes = methods.changes
                    val egr = methods.egr
                    val check = methods.check
                    val multiInfo = methods.multinfo
                    val search = methods.search
                    var egrString = "Данные о компании: "
                    var changesString = "\n\nОтслеживание изменений: "
                    var checkString = "\n\nПроверка контрагента: "
                    var multiinfoString = "\n\nРеквизиты группы компании: "
                    if (egr != null) {
                        val limit = egr.Лимит ?: ""
                        val spent = egr.Истрачено ?: ""
                        egrString += String.format(getString(R.string.request_statistic_main_string), limit, spent)
                    }
                    if (changes != null) {
                        val limit = changes.Лимит ?: ""
                        val spent = changes.Истрачено ?: ""
                        changesString += String.format(getString(R.string.request_statistic_main_string), limit, spent)
                    }
                    if (check != null) {
                        val limit = check.Лимит ?: ""
                        val spent = check.Истрачено ?: ""
                        checkString += String.format(getString(R.string.request_statistic_main_string), limit, spent)
                    }
                    if (multiInfo != null) {
                        val limit = multiInfo.Лимит ?: ""
                        val spent = multiInfo.Истрачено ?: ""
                        multiinfoString += String.format(getString(R.string.request_statistic_main_string), limit, spent)
                    }
                    tvContent?.text = TextUtils.createSpannableString(egrString + changesString + checkString + multiinfoString)
                }
            }
        })
    }
}