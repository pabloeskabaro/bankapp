package com.example.vlad.bankapp.dto

import com.google.gson.JsonObject
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName

data class BaseCompanyData(val items: List<Item28036961>?) {

    data class Item28036961(
            @SerializedName("ЮЛ")
            val ul: Ul?) {

        data class Ul(
                @SerializedName("ИНН")
                val inn: String?,
                val КПП: String?,
                val ОГРН: String?,
                val ДатаРег: String?,
                val ОКОПФ: String?,
                val Статус: String?,
                val СпОбрЮЛ: String?,
                @SerializedName("НО")
                val no: НО?,
                @SerializedName("ПФ")
                val pf: ПФ?,
                @SerializedName("ФСС")
                val fss: ФСС?,
                @SerializedName("Капитал")
                val capital: Капитал?,
                val НаимСокрЮЛ: String?,
                val НаимПолнЮЛ: String?,
                val Адрес: Adress?,
                @SerializedName("Руководитель")
                val boss: Руководитель?,
                val Учредители: List<Учредители850913484>?,
                @SerializedName("Лицензии")
                val license: List<License>,
                @SerializedName("ОснВидДеят")
                val doingType: ОснВидДеят?,
                val ДопВидДеят: List<ДопВидДеят1752390421>?,
                val СПВЗ: List<СПВЗ1044923791>?,
                @SerializedName("Филиалы")
                val filials: List<Filial>?,
                @SerializedName("ОткрСведения")
                val openInfo: ОткрСведения?,
                val Участия: List<Участия2080825467>?,
                @SerializedName("НомТел")
                val phoneNumber: String?,
                @SerializedName("E-mail")
                val email: String?,
                @SerializedName("История")
                val story: История?) {

            data class License(val НомерЛиц: String?, val ВидДеятельности: String?, val ДатаНачала: String?, val ДатаОконч: String?, val МестоДейств: String?)

            data class Filial(@SerializedName("Тип")val type: String?, @SerializedName("Адрес") val adress: String?)

            data class Руководитель(val ВидДолжн: String?= "", val Должн: String?= "", val ФИОПолн: String?= "", val ИННФЛ: String?= "", val Дата: String?= "")

            data class Adress(
                    @SerializedName("АдресПолн")
                    val fullName: String?)

            //2015-03-26 ~ 2018-10-02
            data class Name(
                    @SerializedName("ФИОПолн")
                    val fullName: String?,
                    @SerializedName("ИННФЛ")
                    val Innfl: String?)

            data class ФСС(val РегНомФСС: String?, val ДатаРегФСС: String?, val КодФСС: String?)

            data class ДопВидДеят1752390421(val Код: String?, val Текст: String?)

            data class Капитал(val ВидКап: String?, val СумКап: String?, val Дата: String?)

            data class НО(val Рег: String?, val РегДата: String?, val Учет: String?, val УчетДата: String?)

            data class Налоги1572388903(val НаимНалог: String?, val СумНедНалог: String?, val СумПени: String?, val СумШтраф: String?, val ОбщСумНедоим: String?)

            data class ОснВидДеят(val Код: String?, val Текст: String?)

            data class ОткрСведения(val КолРаб: String?, val СведСНР: String?, val ПризнУчКГН: String?, @SerializedName("Налоги") val taxs: List<Tax>?, val СумДоход: String?, val СумРасход: String?,
                    @SerializedName("ОтраслевыеПок") val industryIndications: ОтраслевыеПок?, val Дата: String?) {

                data class Tax(@SerializedName("НаимНалог") val taxName: String?,
                        @SerializedName("СумУплНал")val paidSum: String?,
                        @SerializedName("СумНедНалог")val nonPaidSum: String?,
                        @SerializedName("СумПени") val peniSum: String?,
                        @SerializedName("СумШтраф") val fine: String?,
                        @SerializedName("ОбщСумНедоим") val nonPaidFineSum: String?)

                data class ОтраслевыеПок(val НалогНагрузка: String?, val Рентабельность: String?)
            }

            data class ПФ(val РегНомПФ: String?, val ДатаРегПФ: String?, val КодПФ: String?)

            data class СПВЗ1044923791(val Дата: String?, val Текст: String?)

            data class Участия2080825467(val ОГРН: String?, val ИНН: String?, val НаимСокрЮЛ: String?, val НаимПолнЮЛ: String?, val Статус: String?, val Процент: String?, val СуммаУК: String?)

            data class Учредители850913484(@SerializedName("УчрФЛ") val uchrFl: УчрФЛ?, val СуммаУК: String?, val Процент: String?, val Дата: String?) {
                data class УчрФЛ(val ФИОПолн: String?, val ИННФЛ: String?)
            }

            data class История(
                    @SerializedName("НомТел")
                    val phoneNumber: JsonObject? = JsonObject(),
                    @SerializedName("E-mail")
                    val email: JsonObject? = JsonObject(),
                    @JsonAdapter(StoryBossDeserializer::class)
                    @SerializedName("Руководитель")
                    val boss: Map<String, Руководитель>?,
                    val Адрес: JsonObject? = JsonObject()) {

                data class AddressMain(
                        @SerializedName("2003-09-08 ~ 2016-09-22")
                        val adress: Adress?,
                        @SerializedName("2016-09-23 ~ 2017-12-11")
                        val adress2: Adress2?) {

                    data class Adress(
                            @SerializedName("АдресПолн")
                            val fullName: String?)

                    data class Adress2(
                            @SerializedName("АдресПолн")
                            val fullAddress: String?)
                }


                data class НомТел(
                        @SerializedName("2015-03-26 ~ 2017-12-11")
                        val phoneNumber: String?)

                //E-mail
                data class Email(
                        @SerializedName("2016-09-23 ~ 2017-12-11")
                        val mail: String?)
            }
        }
    }
}




