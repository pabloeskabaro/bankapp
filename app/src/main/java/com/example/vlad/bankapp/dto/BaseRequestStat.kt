package com.example.vlad.bankapp.dto

import com.google.gson.annotations.SerializedName

data class BaseRequestStat(val ДатаНач: String?, val ДатаОконч: String?, @SerializedName("Методы")val methods: Методы?) {

    data class Методы(val changes: Changes?, val check: Check?, val egr: Egr?, val multinfo: Multinfo?, val search: Search?) {
        data class Changes(val Лимит: String?, val Истрачено: String?)

        data class Check(val Лимит: String?, val Истрачено: String?)

        data class Egr(val Лимит: String?, val Истрачено: String?)

        data class Multinfo(val Лимит: String?, val Истрачено: String?)

        data class Search(val Лимит: String?, val Истрачено: String?)
    }

}