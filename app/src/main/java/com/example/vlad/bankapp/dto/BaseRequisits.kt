package com.example.vlad.bankapp.dto

import com.google.gson.annotations.SerializedName

data class BaseRequisits(val items: List<Items1024641889>?) {
    data class Items1024641889(@SerializedName("ЮЛ") val ul: ЮЛ?) {
        data class ЮЛ(val ИНН: String?,
                val КПП: String?,
                val ОГРН: String?,
                val ДатаРег: String?,
                val Статус: String?,
                @SerializedName("Капитал")
                val capital: Капитал?,
                val НаимСокрЮЛ: String?,
                val НаимПолнЮЛ: String?,
                @SerializedName("ДатаПрекр")
                val endOfActivityDate: String?,
                @SerializedName("Адрес")
                val adress: Адрес?,
                @SerializedName("Руководитель")
                val boss: Руководитель?,
                @SerializedName("ОснВидДеят")
                val mainTypeOfActivity: ОснВидДеят?) {

            data class Адрес(val КодРегион: String?, val Индекс: String?, val АдресПолн: String?)

            data class Капитал(val СумКап: String?)

            data class ОснВидДеят(val Код: String?, val Текст: String?)

            data class Руководитель(val ФИОПолн: String?)
        }
    }
}