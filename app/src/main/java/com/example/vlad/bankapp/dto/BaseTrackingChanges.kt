package com.example.vlad.bankapp.dto

import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName

data class BaseTrackingChanges(val items: List<Items1547949463>?) {

    data class Items1547949463(
            @SerializedName("ЮЛ")
            val ul: Ul?) {

        data class Ul(
                @SerializedName("ОГРН") val ogrn: String?,
                @SerializedName("ИНН") val inn: String?,
                @JsonAdapter(ChangesDeserialize::class)
                @SerializedName("Изменения")
                val changes: Map<String, Change>?) {

            data class Change(
                    @SerializedName("Статус")
                    val status: String?,
                    @SerializedName("НО")
                    val taxInfo: No?,
                    @SerializedName("ФСС")
                    val fssInfo: Fss?,
                    @SerializedName("Адрес")
                    val adress: Adress?,
                    @SerializedName("Капитал")
                    val capital: Capital?,
                    @SerializedName("Руководитель")
                    val boss: Boss?,
                    @SerializedName("СПВЗ")
                    val spvz: String?) {

                data class Capital(@SerializedName("СумКап") val capSum: String?)
                data class Boss(@SerializedName("ФИОПолн") val fullName: String?, @SerializedName("ИННФЛ") val innfl: String?)
                data class No(@SerializedName("Рег") val reg: String?, @SerializedName("Учет") val accounting: String?)
                data class Fss(@SerializedName("РегНомФСС") val fssRegNumber: String?)
                data class Adress(@SerializedName("АдресПолн") val fullAdress: String?)
            }
        }
    }
}

