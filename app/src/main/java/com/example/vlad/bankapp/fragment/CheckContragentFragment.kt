package com.example.vlad.bankapp.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.vlad.bankapp.R
import com.example.vlad.bankapp.Api
import com.example.vlad.bankapp.App
import com.example.vlad.bankapp.TextUtils
import com.example.vlad.bankapp.activity.MainActivity
import com.example.vlad.bankapp.dto.BaseContractor
import kotlinx.android.synthetic.main.fragment_check_contragent.etINNInput
import kotlinx.android.synthetic.main.fragment_company_data.btnSearch
import kotlinx.android.synthetic.main.fragment_company_data.tvContent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CheckContragentFragment : BaseFragment() {

    private var inn: String = ""

    private val TAG: String = javaClass.simpleName

    private lateinit var api: Api

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_check_contragent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setToolbarTitle("Проверка контрагента")
        api = (activity?.application as App).api
        btnSearch.setOnClickListener {
            inn = etINNInput.text.toString()
            initData(inn)
        }
    }

    override fun refreshData() {
        if (!android.text.TextUtils.isEmpty(inn)) initData(inn)
    }

    private fun initData(inn: String) {
        api.checkContractor(inn, (activity as MainActivity).getKey()).enqueue(object : Callback<BaseContractor> {
            @SuppressLint("SetTextI18n")
            override fun onFailure(call: Call<BaseContractor>, t: Throwable) {
                Log.d(TAG, t.message)
                tvContent?.text = "Error"
            }

            override fun onResponse(call: Call<BaseContractor>, response: Response<BaseContractor>) {
                val ulInfo = response.body()?.items?.get(0)?.ul ?: return
                val innString = ulInfo.inn
                val ogrn = ulInfo.ogrn
                val positive = ulInfo.positive
                val negative = ulInfo.negative
                val positiveInfo = "\nЛицензии: ${positive?.license ?: ""}" +
                        "\nФилиалы: ${positive?.filials ?: ""}" +
                        "\nКапитал более 50 тысяч: ${positive?.capMore50k ?: ""}" +
                        "\nТекст: ${positive?.text ?: ""}"
                val negativeInfo = "\nМасс Адрес: ${negative?.massAdress ?: ""}" +
                        "\nТекст: ${negative?.text ?: ""}"
                val text = TextUtils.createSpannableString(
                        String.format(getString(R.string.check_contractor_main_string), innString, ogrn, positiveInfo, negativeInfo))
                tvContent?.text = text
            }
        })
    }
}